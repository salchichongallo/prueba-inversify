import { Container, injectable, inject, multiInject } from 'inversify';
import 'reflect-metadata';

interface Weapon { name: string; }

@injectable() class Katana implements Weapon { public name = 'katana'; }
@injectable() class Shuriken implements Weapon { public name = 'shuriken'; }

interface NinjaContract { katana: Weapon; shuriken: Weapon; }

@injectable()
class Ninja implements NinjaContract {
  public katana: Weapon;
  public shuriken: Weapon;
  constructor(@multiInject('Weapon') weapons: Weapon[]) {
    console.log(weapons[0].name);
    console.log(weapons[1].name);
  }
}

let container = new Container;
container.bind(Ninja).toSelf();
container.bind('Weapon').to(Katana);
container.bind('Weapon').to(Shuriken);

container.get(Ninja);